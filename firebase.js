// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.9.1/firebase-app.js";
import { 
    getFirestore, 
    collection, 
    addDoc, 
    getDocs, 
    onSnapshot,
    deleteDoc,
    doc,
    getDoc,
    updateDoc,
} from "https://www.gstatic.com/firebasejs/9.9.1/firebase-firestore.js"
// TODO: Add SDKs for Firebase products that you want to use

/* Si quisieramos conectarnos a la base de datos tenemos que visitar el siguiente enlace, y seleccionar el servicio de firebase */
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyATzex2lA0n0Wx3S41kOERF-LDUcIPE8dI",
    authDomain: "fireb-js-crud.firebaseapp.com",
    projectId: "fireb-js-crud",
    storageBucket: "fireb-js-crud.appspot.com",
    messagingSenderId: "681786375729",
    appId: "1:681786375729:web:405be4148692988e349f10"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getFirestore()

export const saveTask = (title, description) => addDoc(collection(db, 'tasks'), {title, description})

export const getTasks = () => getDocs(collection(db, 'tasks'))


export const onGetTasks = (callback) => onSnapshot(collection(db, 'tasks'), callback)

export const deleteTask = id => deleteDoc(doc(db, 'tasks', id))

export const getTask = id => getDoc(doc(db, 'tasks', id))

export const updateTask = (id, newFields) => updateDoc(doc(db, 'tasks', id), newFields)